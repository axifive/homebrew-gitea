class Tea < Formula
  desc "A command line tool to interact with Gitea servers"
  homepage "https://gitea.com/gitea/tea"
  version "0.8.0"

  os = OS.mac? ? "darwin" : "linux"
  arch = case Hardware::CPU.arch
         when :x86_64 then "amd64"
         when :arm64 then "arm64"
         else
           raise "tea: Unsupported system architecture #{Hardware::CPU.arch}"
         end

  @@filename = "tea-#{version}-#{os}-#{arch}"
  @@url = "https://dl.gitea.io/tea/#{version}/#{@@filename}"
  
  if os == "darwin" || arch == "amd64"
    @@url += ".xz"
    depends_on "xz"

    url @@url
  else
    url @@url,
      using: :nounzip
  end

  @@sha256 = case "#{os}-#{arch}"
             when "linux-amd64" then "29976cc605f0da406efdc010d4a63ff225f990ebb49efe9f54ecf5c5796e771e"
             when "linux-arm64" then "f85754008200f4ce3a1de50f77eb72d4960fffd069d19c0655376357a70a5226"
             when "darwin-amd64" then "8d9aaef2c9e851759a575892d5af8dd2130f0b9c5705189572a282f812126a48"
             when "darwin-arm64" then "fb525636eae8a7a1af1c5799419b5017e3951d6a48c60831d6d01bd4811e2ef5"
             else
               raise "tea: Unsupported system #{os}-#{arch}"
             end

  sha256 @@sha256

  conflicts_with "tea-head", because: "both install tea binaries"
  bottle :unneeded
  def install
    filename = Tea.class_variable_get("@@filename")
    bin.install filename => "tea"
  end

  test do
    system "#{bin}/tea", "--version"
  end
end
